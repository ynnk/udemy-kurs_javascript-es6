/**
 * CONSOLE LOGGING
 * As the followings are console methods, see the console output for more details
 */
console.log("%cStart some useful log methods", "background: #000; color: #0f0; font-size: 16px");
console.table({ a: 1, b: 2, c: 3 });
console.error("Error Message!");
console.warn("Warn Message!");
console.info("Info Message!");
console.groupCollapsed("execution time");
console.time("executin time");
for (i = 0; i < 10; i++) {
    console.log("get execution time...");
};
console.groupEnd("execution time");
console.timeEnd("executin time");
console.assert(1 > 2, "1>2 - This is false");
for (i = 0; i < 3; i++) {
    consoleCount();
};
function consoleCount() {
    console.count("function consoleCount() already ran", " times");
}
console.log("%cEnd of log methods", "background: #000; color: #f00; font-size: 16px");
